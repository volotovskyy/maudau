var button = document.querySelector('#copy-promo-code-to-clipboard-button');
var tooltip = document.querySelector('#copied-tooltip');

Popper.createPopper(button, tooltip, {
    placement: 'top',
    modifiers: [
      {
        name: 'offset',
        options: {
          offset: [0, 8],
        },
      },
    ],
  });

var clipboard = new ClipboardJS('.promo-code-group__copy__btn');

var tooltipActiveClass = 'tooltip-visible'

function hideTooltip() {
  setTimeout(function() {
    tooltip.classList.remove(tooltipActiveClass)
  }, 1500);
}

clipboard.on('success', function(e) {
  tooltip.classList.add(tooltipActiveClass)
  hideTooltip();
});

$(document).ready(function() {
    $('.promo-slider').slick({
      speed: 700,
      slidesToShow: 4,
      slidesToScroll: 4,
      arrows: true,
      prevArrow: '<div class="slick-arrow slider-arrow slider-arrow--left"></div>',
      nextArrow: '<div class="slick-arrow slider-arrow slider-arrow--right"></div>',
      dots: false,
      infinite: true,
      responsive: [
        {
            breakpoint: 1200,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1,
            }
        },
        {
            breakpoint: 920,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
            }
        },
        {
            breakpoint: 599,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
            }
        },
      ]
    });
  })

$(document).ready(function() {
  $('.main-slider').slick({
    speed: 700,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    prevArrow: '<div class="slick-arrow slider-arrow slider-arrow--left"></div>',
    nextArrow: '<div class="slick-arrow slider-arrow slider-arrow--right"></div>',
    dots: true,
    infinite: true,
    responsive: [
      {
          breakpoint: 920,
          settings: {
              dots: false
          }
      },
    ]
  });
})
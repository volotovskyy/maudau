$(document).ready(function() {
    $('.buttons-slider').slick({
        speed: 700,
        slidesToShow: 3,
        slidesToScroll: 3,
        arrows: true,
        dots: false,
        infinite: true,
        prevArrow: '<div class="slick-arrow slider-arrow slider-arrow--left"></div>',
        nextArrow: '<div class="slick-arrow slider-arrow slider-arrow--right"></div>',
        responsive: [{
                breakpoint: 1200,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                }
            },
            {
                breakpoint: 920,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                }
            },
            {
                breakpoint: 545,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }
            },
        ]
    });
})